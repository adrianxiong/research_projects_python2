from __future__ import print_function
import argparse
import sys
import numpy as np


def main():
    parser = argparse.ArgumentParser(description="Generate a random matrix.")
    parser.add_argument('nrows', type=int, help="Number of rows.")
    parser.add_argument('ncols', type=int, help="Number of columns.")
    parser.add_argument('-output', type=argparse.FileType('w'), default=sys.stdout)
    parser.add_argument('-low', type=float, help='Lower bound.', default=0)
    parser.add_argument('-high', type=float, help='Higher bound.', default=1)
    ns = parser.parse_args()
    low = ns.low
    high = ns.high
    size = ns.ncols
    handle = ns.output
    for iRow in xrange(ns.nrows):
        print('\t'.join(map(str, np.random.uniform(low, high, size))), file=handle)

if __name__ == '__main__':
    main()