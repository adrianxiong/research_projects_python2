from __future__ import print_function
import argparse
import os
import numpy as np
import pandas as pd


def handel(ns):
    """
    :param ns: parsed namespace containing parameters.
    """

    def normalize(array):
        # Each array passed in is a Series
        return np.interp(array, [array.min(), array.max()], [0, 1])

    def get_stages():
        stages = []
        with open(ns.stage_definition) as reader:
            for line in reader:
                name, conditions = line.split('\t', 1)
                conditions = conditions.split()
                stages.append((name, conditions))
        return stages

    def get_normalized_df(all_conditions):
        # print ("all_conditions", all_conditions)
        raw_df = pd.read_table(ns.expression_profile, index_col=0)
        raw_df = raw_df[all_conditions]
        raw_df = raw_df.apply(normalize, axis=1)
        return raw_df

    def pick_max_expression_in_stages(expr_df, stages):
        result = pd.DataFrame()
        for stage_name, conditions in stages:
            result[stage_name] = expr_df.ix[:, conditions].apply(max, axis=1)
        return result

    def output_edges(gene_max_expression_in_stages_df, stages, threshold, writer):
        for stage_name, conditions in stages:
            col_stage = gene_max_expression_in_stages_df[stage_name]
            col_stage = col_stage[col_stage >= threshold]
            for index, value in col_stage.iteritems():
                print(index, stage_name, value, file=writer, sep='\t')


    stages = get_stages()
    all_conditions = [j for i in stages for j in i[1]]
    normalized_df = get_normalized_df(all_conditions)

    # print (stages)
    # print (normalized_df)
    # print (pick_max_expression_in_stages(normalized_df, stages))
    max_gene_expr = pick_max_expression_in_stages(normalized_df, stages)
    with open(ns.output, 'w') as writer:
        output_edges(max_gene_expr, stages, ns.t, writer)

def _readable_file(string):
    if os.path.isfile(string):
        return string
    else:
        raise argparse.ArgumentTypeError("{} is NOT a valid regular file path.".format(string))


def _writable_file(string):
    try:
        with open(string, 'w'):
            pass
        os.remove(string)
        return string
    except BaseException as e:
        raise argparse.ArgumentTypeError("{} is NOT a writable file path. \nERROR: {}".format(string, e))


def main():
    """
    Input-1: a gene expression profile, where each row is a gene, and each column is a condition (e.g. time point).
    Input-2: a stage definition file, where each row is a stage name + TAB + [column indices + SPACE]+
    Input-3: a threshold of relative intensity normalized by maximum, e.g. 0.3
    Output: a file of gene-stage mapping. Each row is a gene + TAB + stage. One gene can have multiple stages.
    """
    parser = argparse.ArgumentParser(description="Determine gene-stage association from gene relative expression.")
    parser.add_argument("-t", "-threshold", type=float, help="a threshold of relative intensity normalized by maximum.", default=0.3)
    parser.add_argument("expression_profile", type=_readable_file,
        help="a gene expression profile, where each row is a gene, and each column is a condition (e.g. time point).")
    parser.add_argument("stage_definition", type=_readable_file,
        help="a stage definition file, where each row is a stage name + TAB + [column indices + SPACE]+")

    parser.add_argument('output', type=_writable_file,
        help="Output file for gene-stage mapping. Each row is a gene + TAB + stage. One gene can have multiple stages")
    ns = parser.parse_args()
    handel(ns)


if __name__ == '__main__':
    main()
