import argparse
import os
import numpy as np
from scipy.interpolate import splev
from scipy.stats import entropy


def calculate_MI(ns):
    """
    Main function to calculate the MI matrix
    :param ns: parsed namespace from cmd line.
    """
    M = ns.M
    k = ns.k - 1

    # Calculate the bin distribution for each data point
    def get_bin():
        # Linearly transform every gene's expression intensities to a [0, 1] range.
        def normalize(array):
            return np.interp(array, [array.min(), array.max()], [0, 1])

        raw_gene_expression = np.loadtxt(ns.gene_expression)
        raw_gene_expression = np.apply_along_axis(normalize, 1, raw_gene_expression)
        nGene, nCondition = np.shape(raw_gene_expression)

        knots = np.concatenate((np.zeros(k), np.linspace(0, 1, M - k + 1), np.ones(k)))
        adjusted_coe = []
        for i in xrange(M):
            coe = np.zeros(M+k+1)
            coe[i] = 1.0
            adjusted_coe.append(coe)

        rst = np.empty([nGene, M, nCondition])
        for iRow, row in enumerate(raw_gene_expression):
            rst[iRow] = np.asarray([splev(row, (knots, adjusted_coe[i], k)) for i in xrange(M)])

        return rst

    def bin_to_MI(matrix):
        nrows = matrix.shape[0]
        result = np.zeros((nrows, nrows), dtype=np.float)
        for i in xrange(nrows):
            marginal_entropy_m = entropy(matrix[i].sum(axis=1), base=2.0)
            for j in xrange(nrows):
                if i > j:
                    result[i, j] = result[j, i]
                    continue
                marginal_entropy_n = entropy(matrix[j].sum(axis=1), base=2.0)
                joint_distribution = np.dot(matrix[i], matrix[j].transpose())
                joint_entropy = entropy(joint_distribution.flat, base=2.0)
                result[i, j] = marginal_entropy_m + marginal_entropy_n - joint_entropy
        return result

    MI_matrix = bin_to_MI(get_bin())
    np.savetxt(ns.MI_output, MI_matrix, delimiter="\t", fmt="%.8f")


def _readable_file(string):
    if os.path.isfile(string):
        return string
    else:
        raise argparse.ArgumentTypeError("{} is NOT a valid regular file path.".format(string))


def _writable_file(string):
    try:
        with open(string, 'w'):
            pass
        os.remove(string)
        return string
    except BaseException as e:
        raise argparse.ArgumentTypeError("{} is NOT a writable file path. \nERROR: {}".format(string, e))


def main():
    parser = argparse.ArgumentParser(description=""
        "Calculate a mutual information matrix from a gene expression profile.")
    parser.add_argument('-k', '-order', type=int, help="B-Spline order. Must >= 2", default=3)
    parser.add_argument('-M', '-bin', type=int, help="Number of bins.", default=10)
    parser.add_argument('gene_expression', type=_readable_file, help=""
        "Gene expression file, where each row is a gene and each column is a condition.")
    parser.add_argument('MI_output', type=_writable_file, help="Output file for the MI matrix.")
    ns = parser.parse_args()
    calculate_MI(ns)


if __name__ == '__main__':
    main()
