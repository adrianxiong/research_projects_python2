import numpy as np
from scipy.interpolate import splev, splrep, splprep
from scipy.stats import entropy
import time


def load_matrix(file_path):
    return np.loadtxt(file_path)


def log(info):
    print "[{}]\t{}".format(time.strftime("%Y-%m-%d %H:%M:%S"), info)


def to_bin(matrix, k, M):
    nGene, nCondition = np.shape(matrix)

    def get_tck(M, k):
        knots = np.concatenate((np.zeros(k - 1), np.linspace(0, 1, M - k + 2), np.ones(k - 1)))
        coe = np.concatenate((np.ones(M), np.zeros(k)))
        return (knots, coe, k - 1)

    def convert_bins(data):
        return np.asarray([splev(data, (knot, adjusted_coe[i], k)) for i in range(M)])

    knot, coe, k = get_tck(M, k)
    adjusted_coe = []
    for i in xrange(M):
        single_coe = np.zeros_like(coe)
        single_coe[i] = 1.0
        adjusted_coe.append(single_coe)

    rst = np.empty([nGene, M, nCondition])
    for i, row in enumerate(matrix):
        rst[i] = convert_bins(row)
    return rst


def cal_MI(matrix):
    nrows = matrix.shape[0]
    result = np.zeros((nrows, nrows), dtype=np.float)
    for i in xrange(nrows):
        marginal_entropy_m = entropy(matrix[i].sum(axis=1), base=2.0)
        log(marginal_entropy_m)
        for j in xrange(nrows):
            if i == j: continue
            elif i > j:
                result[i,j] = result[j,i]
                continue
            marginal_entropy_n = entropy(matrix[j].sum(axis=1), base=2.0)
            joint_distribution = np.dot(matrix[i], matrix[j].transpose())
            joint_entropy = entropy(joint_distribution.flat, base=2.0)
            result[i, j] = marginal_entropy_m + marginal_entropy_n - joint_entropy
    return result


def normalize(array):
    return np.interp(array, [array.min(), array.max()], [0, 1])


def run_paper_example():
    expresssion_profile = np.array([[0.0, 0.2, 0.4, 0.6, 0.8, 1.0], [0.8, 1.0, 0.6, 0.4, 0.0, 0.2]])
    bins = to_bin(expresssion_profile, k=2, M=3)
    print "Bins"
    print bins
    print "MI"
    print cal_MI(bins)


def run():
    expresssion_profile = load_matrix("/Users/xiong/r/network/test_small/gene_expression_raw.tsv")
    expresssion_profile = np.apply_along_axis(normalize, 1, expresssion_profile)

    k, M = 3, 10
    bins = to_bin(expresssion_profile, k=k, M=M)
    MI = cal_MI(bins)
    np.savetxt("/Users/xiong/r/network/test_small/py_MI_full.tsv", MI, delimiter="\t", fmt="%.8f")


def run_files(expression_file, mi_out_file, k=3, M=10):
    expresssion_profile = load_matrix(expression_file)
    expresssion_profile = np.apply_along_axis(normalize, 1, expresssion_profile)

    shape = expresssion_profile.shape
    log("Loading gene expression profile from: {}".format(expression_file))
    log("{} genes and {} conditions.".format(shape[0], shape[1]))

    bins = to_bin(expresssion_profile, k=k, M=M)
    MI = cal_MI(bins)
    np.savetxt(mi_out_file, MI, delimiter="\t", fmt="%.8f")


if __name__ == '__main__':
    # run_paper_example()
    run()
    # run_files("/Users/xiong/r/network/maize_seed_pymi/gene_expression_raw.tsv",
    #           "/Users/xiong/r/network/maize_seed_pymi/py_MI_maize.tsv")
