# Infer gene network from mutual information

## Bin smoothing using B-Spline functions 
### Background
A B-Spline function is a piece-wise function defined by a knot vector (t) and a degree (k). 
It is widely used in **interpolation**, aka. curve fitting. 

### Characteristics of B-Spline functions
- Given a number (_M_) of control points and a degree _k_, the internal values of the curve at _Xi_ are interpolated by
_k_ nearest control points that are no less than _Xi_.
- One control point affects _M-k+1_ intervals(?), the extent or weight of which is determined by the coefficients of B-Spline functions.
- The weight always sums up to unity.
- The weight is used as 

[[1]\] Estimating mutual information using B-spline functions – an improved similarity measure for analysing gene expression data.


[1]: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiJ9sW4z-XQAhUT-mMKHWNvCi8QFggfMAA&url=https%3A%2F%2Fbmcbioinformatics.biomedcentral.com%2Farticles%2F10.1186%2F1471-2105-5-118&usg=AFQjCNE3qGuVmUL3vna-oDYGDxLUSdHuZQ 
