from __future__ import print_function
import argparse
from collections import defaultdict
from itertools import combinations


def main():
    parser = argparse.ArgumentParser(description="Create a network from gene sets.")
    parser.add_argument('input', type=argparse.FileType('r'), help="Input file in TSV. No headers. Col-1: gene id; Col-2: gene set name")
    parser.add_argument('-output_prefix', type=str, default='', help="Output file prefix.")
    parser.add_argument('--gene_attribute', '--ga', action='store_true', help='Generate gene attributes file.')
    parser.add_argument('--sep', help='Seperator in gene attributes.', default=' ')
    ns = parser.parse_args()

    create_gene_attr = ns.gene_attribute
    sep = ns.sep

    # Load genes into sets
    gene_sets = defaultdict(set)
    gene_attr = defaultdict(set)

    for line in ns.input:
        arr = line.split('\t', 2)
        geneId = arr[0]
        geneSetName = arr[1]
        gene_sets[geneSetName].add(geneId)
        if create_gene_attr:
            gene_attr[geneId].add(geneSetName)

    prefix = ns.output_prefix + '_' if ns.output_prefix else ''
    with open(prefix + 'nodes.tsv', 'w') as node_writer, open(prefix + 'edges.tsv', 'w') as edge_writer:
        print('GeneSet', 'Size', sep='\t', file=node_writer)
        print('source_id', 'target_id', 'edge_kind', 'shared_genes', sep='\t', file=edge_writer)

        # Output every gene set
        for name in gene_sets:
            print(name, len(gene_sets[name]), sep='\t', file=node_writer)

        # Compare any two gene sets
        for set1, set2 in combinations(gene_sets.keys(), 2):
            print(set1, set2, 'have_overlap_genes', len(gene_sets[set1].intersection(gene_sets[set2])),
                  sep='\t', file=edge_writer)

    if create_gene_attr:
        with open(prefix + 'gene.attr.tsv', 'w') as writer:
            print('GeneName', 'InSets', sep='\t', file=writer)
            for gene, attrs in gene_attr.iteritems():
                print(gene, sep.join(attrs), sep='\t', file=writer)

if __name__ == '__main__':
    main()