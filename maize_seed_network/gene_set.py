from __future__ import print_function
import pandas as pd


class GeneSet(set):
    def __init__(self, name, *args, **kwargs):
        self.name = name
        super(GeneSet, self).__init__(*args, **kwargs)
        self.size = len(self)

    separator = '|'

    def __repr__(self):
        return "GeneSet {} [{}]: {}".format(self.name, len(self), " ".join(self))

    def intersection(self, *args, **kwargs):
        """Multiple GeneSets are allowed in *args"""
        new_name = '&'.join([self.name] + [i.name for i in args])
        genes = super(GeneSet, self).intersection(*args, **kwargs)
        return GeneSet(new_name, genes)

    def substract(self, other):
        new_name = "{}-{}".format(self.name, other.name)
        genes = self - other
        return GeneSet(new_name, genes)

    def to_tabular(self):
        return "{}\t{}\t{}".format(self.name, self.size, self.separator.join(sorted(self)))


class GeneSetCollection(object):
    def __init__(self, names, dict_data):
        """
        :param names: used to track the order of gene set names 
        :param dict_data: internal dict, key as gene set name and value as gene set
        """
        self.names = names
        self._dict_genesets = dict_data
        # Annotations of gene sets, gene set names as keys and DataFrame as values
        self.annotation = {}

    separator = '|'

    def __getitem__(self, name):
        return self._dict_genesets[name]

    def write_to(self, handle):
        for name in self.names:
            geneSet = self[name]
            print(geneSet.to_tabular(), file=handle)

    def annotate_with(self, dataframe):
        for name in self.names:
            geneset = self[name]
            df_geneset = dataframe.ix[geneset]
            self.annotation[name] = df_geneset


    @classmethod
    def process_handle(cls, handle_or_filepath, fn, *args, **kwargs):
        if isinstance(handle_or_filepath, str):
            with open(handle_or_filepath) as handle:
                result = fn(handle, *args, **kwargs)
        else:
            result = fn(handle_or_filepath, *args, **kwargs)
        return result


    @classmethod
    def load_from_regular_file(cls, handle_or_filepath):
        def inner(handle):
            names = []
            dict_data = {}
            for line in handle:
                arr = line[:-1].split('\t')
                name = arr[0]
                genes = arr[-1].split(GeneSetCollection.separator)
                names.append(name)
                dict_data[name] = GeneSet(name, genes)
            return GeneSetCollection(names, dict_data)

        return GeneSetCollection.process_handle(handle_or_filepath, inner)

    @classmethod
    def load_from_community_file(cls, handle_or_filepath):
        def inner(handle):
            names = []
            dict_data = {}
            mi_k = "EMPTY"
            for line in handle:
                arr = line[:-1].split('\t')
                if arr[0]:
                    mi_k = arr[0].replace('/', '').replace('=', '')
                else:
                    community_name = mi_k + arr[1]
                    genes = arr[-1].split(GeneSetCollection.separator)
                    names.append(community_name)
                    dict_data[community_name] = GeneSet(community_name, genes)
            return GeneSetCollection(names, dict_data)

        return GeneSetCollection.process_handle(handle_or_filepath, inner)
