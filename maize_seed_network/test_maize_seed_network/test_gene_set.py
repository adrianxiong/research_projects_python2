from maize_seed_network.gene_set import GeneSet
from maize_seed_network.gene_set import GeneSetCollection
import unittest


class TestGeneSet(unittest.TestCase):
    def test_creation_length(self):
        gs = GeneSet("set1", "a b c d e f g".split())
        self.assertEqual(gs.name, "set1")
        self.assertEqual(len(gs), 7)
        self.assertTrue("a" in gs)

    def test_intersection(self):
        gs1 = GeneSet("set1", "a b c d e f g".split())
        gs2 = GeneSet("set2", "a c f A B C".split())
        gs_intersection = gs1.intersection(gs2)
        self.assertEqual(len(gs_intersection), 3)
        self.assertEqual(gs_intersection.name, "set1&set2")

    def test_substraction(self):
        gs1 = GeneSet("set1", "a b c d e f g".split())
        gs2 = GeneSet("set2", "a c f A B C".split())
        sub = gs1.substract(gs2)
        print sub
        self.assertEqual(len(sub), 4)
        self.assertEqual(sub.name, "set1-set2")


class TestGeneSetCollection(unittest.TestCase):
    def test_load_from_file(self):
        gsc = GeneSetCollection.load_from_community_file("maize_seed_network/test_maize_seed_network/communities.tsv")
        self.assertEqual(len(gsc.names), len(gsc.dict_data),
                         "Count of names euqal to count of records in a gene set collection")
        for name in gsc.names:
            geneset = gsc.dict_data[name]
            self.assertEqual(name, geneset.name, "GeneSet name should be consistent with GeneSetCollection.names")
            # print "Name:{}, Size:{}, GeneSet:{}".format(name, geneset.size, geneset)


def reorganize_communities():
    with open("/Users/xiong/mycent/Work/research/maize-seed-network/community/all-communities.tsv") as reader, \
            open("/Users/xiong/mycent/Work/research/maize-seed-network/community/all-communities-copywritten.tsv", 'w') as writer:
        gsc = GeneSetCollection.load_from_regular_file(reader)
        gsc.write_to(writer)


def write_figure_communities():
    gsc = GeneSetCollection.load_from_regular_file(
        "/Users/xiong/mycent/Work/research/maize-seed-network/community/all-communities.tsv")
    major_community_names = "mi5k5C0 mi5k5C2 mi5k5C8 mi5k5C9 mi5k5C3 mi5k5C4 mi5k5C14 mi5k5C15 mi5k5C6".split()
    sub_community_names = "mi6k5C0 mi6k5C1 mi5k6C1 mi5k6C4 mi5k6C0 mi5k6C6".split()
    # for sub_name in sub_community_names:
    #     sub_comm = gsc[sub_name]
    #     for major_name in major_community_names:
    #         major_comm = gsc[major_name]
    #         if sub_comm.issubset(major_comm):
    #             print "{} => {}".format(sub_name, major_name)

    major_comms = [gsc[i] for i in major_community_names]
    sub_comms = [gsc[i] for i in sub_community_names]

    for sub in sub_comms:
        for index in xrange(len(major_comms)):
            maj = major_comms[index]
            if sub.issubset(maj):
                major_comms[index] = maj.substract(sub)

    names = []
    dict_genesets = {}
    for geneset in sub_comms + major_comms:
        names.append(geneset.name)
        dict_genesets[geneset.name] = geneset
    new_geneset_collection = GeneSetCollection(names, dict_genesets)

    with open("/Users/xiong/mycent/Work/research/maize-seed-network/community/figure-communities.tsv", 'w') as writer:
        new_geneset_collection.write_to(writer)

def annotate_communities(annotation_file, community_file, out_excel_file):
    import pandas as pd
    annotation = pd.read_table(annotation_file, index_col=0)
    geneset_collection = GeneSetCollection.load_from_regular_file(community_file)
    geneset_collection.annotate_with(annotation)
    writer = pd.ExcelWriter(out_excel_file, engine="xlsxwriter")
    for name in geneset_collection.names:
        community_annotation = geneset_collection.annotation[name]
        community_annotation.to_excel(writer, sheet_name=name)

if __name__ == '__main__':
    # unittest.main()
    # reorganize_communities()
    # write_figure_communities()
    # annotate_communities(
    # '/Users/xiong/mycent/Work/research/maize-seed-network/annotation/overall.genes.annotation.tsv',
    # "/Users/xiong/mycent/Work/research/maize-seed-network/community/all-communities.tsv",
    # "/Users/xiong/mycent/Work/research/maize-seed-network/community/all-communities-annotation.xlsx"
    # )
    annotate_communities(
    '/Users/xiong/mycent/Work/research/maize-seed-network/annotation/overall.genes.annotation.tsv',
    "/Users/xiong/mycent/Work/research/maize-seed-network/community/figure-communities.tsv",
    "/Users/xiong/mycent/Work/research/maize-seed-network/community/figure-communities-annotation.xlsx"
    )
